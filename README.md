# FSFE reverse proxy to external websites

This repository holds the deployment code for containers used to proxy requests
to a remote server from [lund's reverse proxy](https://git.fsfe.org/fsfe-system-hackers/reverse-proxy).

## What does this do?

The containers transfer requests to remote hosts. For example, for the
[pdfreaders.org](pdfreaders.org) website, the requests are transferred from the
reverse proxy to the claus server:

    |                lund                      |        claus          |
    reverse-proxy    ->    proxy-pdfreaders    ->    claus.fsfeurope.org

The benefit of that is that we can use the automatic certificate provisioning
integrated in the reverse proxy for websites not being hosted on lund.

## How does this work?

The containers use [nginx](https://www.nginx.com/) as a reverse proxy. The
[proxy.conf](proxy.conf) file is used as a template for the nginx configuration.
The file [setdest.sh](setdest.sh) replaces the placeholders `%DESTINATION_HOST%`
and `%DESTINATION_PORT%` in the template with environment variables. The
environment variables are set in the [playbook](playbook.yml).

The [Dockerfile](Dockerfile) takes `nginx:alpine` as the base image, exposes the
port 80, and copy both the template and the script to replace the placeholders.
Then it runs the script and start nginx as
[CMD](https://docs.docker.com/engine/reference/builder/#cmd) statement.

The nginx configuration transfers the IP addresses to the web servers as well
as the `Host` HTTP header.
